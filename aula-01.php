<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aula PHP</title>
</head>

<style>
.container{
    width: 500px;
    max-width: 500px;
    margin: 0 auto;
    padding-top: 200px;
  
}

body{
    font-family: Arial;
}

h2{
    font-weight: 500;
    color: grey;
}

form{
    display: flex;
    flex-direction: column;
}

input{
    padding: 10px;
    margin-bottom: 20px;
    border-radius: 4px;
    border: 1px solid #ddd;
}

button{
    width: 100px;
    align-self: flex-end;
    padding: 10px;
    background: grey;
    color: #fff;
    border-radius: 4px;
    border: none;
    cursor: pointer;
}

</style>

<body>


<div class="container">
    <h1>Meu site PHP</h1>
    <?php $StringLogin = "Login"; ?> 
    <h2><?php echo "{$StringLogin}"; ?></h2>
    <form>
        <input type="name" placeholder="Digite seu usuário"> 
        <input type="password" placeholder="Digite sua senha">
        <button>Entrar</button>
    </form>
</div>


</body>
</html>