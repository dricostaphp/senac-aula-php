
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aula 03</title>
</head>

<style>
    body{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }
    .container{
        display: flex;
        width: 100%;
        height: 100vh;
        align-items: center;
        justify-content: center;
        font-size: 14px;
        line-height: 32px;
        color: #333;
        font-weight: 500;
        font-family: Roboto;
    }
</style>
<body>
    
<div class="container"> 
<?php 
    echo "<pre>";
    // Constante no php
    define('QTD_PAGINAS', 10);
    echo "Valor da minha constante é: ". QTD_PAGINAS;

    // Variável para passar valor para uma constante
    $ip_do_banco = '192.168.45.12';
    define('IP_DO_BANCO', $ip_do_banco);
    echo "\nO Ip do banco é: " . IP_DO_BANCO;

    // Constantes de mágicas
    echo "\nestou na linha: " .  __LINE__;
    echo "\nestou na linha: " .  __LINE__;
    echo "\nestou na linha: " .  __LINE__;

    //Muito bom para depurar o código'
    echo "\n";
    var_dump($ip_do_banco);

    //Agora fica mais legal - iremos utilizar Vetores
    $dias_da_semana = ['seg','ter','qua','qui','sex','sab','dom'];

    // var_dump($dias_da_semana);

    unset($dias_da_semana);

    $dias_da_semana = array(0 => 'Dom', 1 => 'Seg', 2 => 'Ter', 3 => 'Qua', 4 => 'Qui', 5 => 'Sex', 6 => 'Sab');

    var_dump($dias_da_semana);

    echo "<pre/>";
?>
</div>

</body>
</html>



