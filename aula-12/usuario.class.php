<?php

class Usuario {

	private $id;
	private $nome;
	private $email;
    private $senha;
    private $conn;
    

	public function __construct(){

        $this->conn = new mysqli('localhost', 'root', '', 'aula_php', 3306);

        if($this->conn){
            // echo "Conectado com sucesso";
        } else {
            die("Problema ao conectar ao SGDB");
        }

	}

	public function setId (int $id) {
		$this->id = $id;
	}

	public function setNome (string $nome) {
		$this->nome = $nome;
	}

	public function setEmail (string $email) {
		$this->email = $email;
	}

	public function setSenha (string $senha) {
		$this->senha = password_hash($senha, PASSWORD_DEFAULT);
	}

	public function getId (int $id): int {
		return $this->id;
	}

	public function getNome (string $nome): string {
		return $this->nome;
	}

	public function getEmail (string $email): string {
		return $this->email;
	}

	public function getSenha (string $senha): string {
		return $this->senha;
    }
    
    public function saveUsuario (){

        $preparada = $this->conn->prepare('REPLACE INTO tb_usuario (id, nome, email, senha) VALUES (?, ?, ?, ?)');

        $preparada->bind_param('isss', 
                                $this->id,
                                $this->nome,
                                $this->email,
                                $this->senha);

        if($preparada->execute()){
            echo "<br><br>Dados do usuario gravados no SGDB!";
        } else {
            echo "não foi possivel gravar informações";
        }
    }

    public function listarUsuario (){

        $prep = $this->conn->prepare('SELECT id, nome, email FROM tb_usuario WHERE id = ?');

        $prep->bind_param('i', $this->id);

        if($prep->execute()){
            echo "<br><br>Dados do usuario listados com sucesso!";
            // var_dump($dados);
        } else {
            echo "não foi possivel listar usuario";
        }

        $result = $prep->get_result();

        $dados = $result->fetch_assoc();

        var_dump($dados);
     
    }

    public function deleteUsuario (){

        $preparada = $this->conn->prepare('DELETE FROM tb_usuario WHERE id= ?');

        $preparada->bind_param('i', $this->id);

        if($preparada->execute()){
            echo "<br><br>Dados do usuario apagados com sucesso!";
        } else {
            echo "não foi possivel apagar usuario";
        }
    }


    public function listarTodosUsuarios(){
        $prep = $this->conn->prepare('SELECT * FROM tb_usuario');

        if($prep->execute()){
            echo "<br><br>Todos os usuarios listados com sucesso!";
        } else {
            echo "não foi possivel listar usuarios";
        }

        $result = $prep->get_result();

        $dados = $result->fetch_all();

        var_dump($dados);
    }




	public function __destruct(){
        unset($this->conn);
		// echo "<br>Fechando a conexão com o SGDB";
	}

}