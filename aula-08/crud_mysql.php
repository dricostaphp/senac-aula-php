
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>


<style>

body{
    box-sizing: border-box;
    padding: 0;
    background: #eee;
    width: 100%;
}

form{
    display: flex;
    flex-direction: column;
    width: 100%;
}

input{
    background: none;
    padding: 15px;
    border: 2px solid #484848;
    border-radius: 4px;
    font-size: 16px;
    outline: none;
}

input[type='submit']{
    width: 100px;
    padding: 10px;
    background: #484848;
    color: #fff;
    font-size: 16px;
    cursor: pointer;
    margin-left: auto;
}

input[type='submit']:hover{
    opacity: 0.8;
}

.container{
    width: 50%;
    margin: 0 auto;
}


tr:nth-child(2n+1){
    background: #484848;
}

tr:nth-child(2n){
    background: rgba(0, 0, 0, 0.3);
}


td:nth-child(2n+1){
    padding: 10px;
    color: #fff;
}

td{
    padding: 5px;
    min-width: 80px;
    white-space: normal;
}

td:nth-child(5){
    display: flex;
}

td:nth-child(5) > button:nth-child(2){
    background: red;
}

button {
    padding: 10px;
    background: green;
    border: none;
    border-radius: 4px;
    color: #fff;
    font-weight: 700;
    cursor: pointer;
    font-size: 12px;
}

button:nth-child(1){
    margin-right: 10px;
}

button:hover{
    opacity: 0.8;
}

label{
    margin-bottom: 10px;
    font-weight: 500;
    font-size: 16px;
    color: #484848;
    font-family: sans-serif;
}

</style>

<body>
<div class="container">
<?php
//Conecta no banco
if( $db = mysqli_connect(	'localhost',
							'root',
							'',
							'aula_php',
							3306) ){

} else {

	die("Problema ao conectar ao SGDB");
}

// funcionalidade de apagar

if( isset($_POST['apagar'])){
    if( is_numeric($_POST['apagar'])){
        
        $prepDelete = mysqli_prepare( $db, 'DELETE FROM tb_bitbucket WHERE id= ?');

        mysqli_stmt_bind_param(	$prepDelete, 'i', $_POST['apagar']);

        mysqli_stmt_execute($prepDelete);
    }
}


$dados['nome'] = null;
$dados['url'] = null;
$id = null;


if ( isset($_POST['editar'])) {
    $id = preg_replace('/\D/', '', $_POST['editar']);

    $prep = mysqli_prepare( $db, '	SELECT nome, url FROM tb_bitbucket WHERE id = ?');

    mysqli_stmt_bind_param($prep, 'i', $id);

    mysqli_stmt_execute($prep);

        $result = mysqli_stmt_get_result($prep);

        $dados = $result->fetch_assoc();

}



//mostra form para o usuário
echo "	<form method='post'>
            <input type='hidden' name='id' value='$id'>
            <label>Nome:</label>
            <input type='text' name='nome' value='{$dados['nome']}'><br>
			<label>Bitbucket:</label> <input type='text' name='url' value='{$dados['url']}'><br>
			<input type='submit' value='salvar'>
		</form>";

//Se o usuário enviar algo, transforma em uma 
//variável mais fácil de escrever		
$nome = isset($_POST['nome']) ? $_POST['nome'] : null;		

$url = isset($_POST['url']) ? $_POST['url'] : null;

if ( empty($_POST['id']) ) {

    //Faz consulta preparada para evitar SQL injection
    $preparada = mysqli_prepare( $db, 'INSERT INTO tb_bitbucket (nome, url, ip) VALUES (?, ?, ?)');

    mysqli_stmt_bind_param(	$preparada, 
                            'sss', 
                            $nome, 
                            $url, 
                            $_SERVER['REMOTE_ADDR']);


    mysqli_stmt_error($preparada);
                            

    if( mysqli_stmt_execute($preparada)){
        echo "<br><br>Dados de $nome gravados no SGDB!";
    } else {
        echo "não foi possivel gravar informações";
    }
    //FIM Faz consulta preparada para evitar SQL injection

} elseif (is_numeric($_POST['id'])) {

    $up = mysqli_prepare($db, 'UPDATE tb_bitbucket SET nome=?, url=?, ip=? WHERE id=?');

    mysqli_stmt_bind_param($up, 'sssi', $nome, $url, $_SERVER['REMOTE_ADDR'], $_POST['id']);


    if(mysqli_stmt_execute($up)){
        echo "<br><br>Dados de $nome atualizados com sucesso no SGDB!";
    }

}
    //Como não tenho dados de usuário (perigoso), não preciso
    //preprar a consulta SQL



$objConsulta = mysqli_query( $db, '	SELECT
										id, 
										nome,
										url,
										ip
									FROM 
                                        tb_bitbucket ');
                                        


echo "<form method='post'>";

echo "<table>
		<tr>
			<td>ID</td>
			<td>Nome</td>
			<td>URL</td>
            <td>IP</td>
            <td></td>
		</tr>";

while($reg = $objConsulta->fetch_assoc()){

    
	echo "<tr>
				<td>{$reg['id']}</td>
				<td>{$reg['nome']}</td>
				<td>{$reg['url']}</td>
                <td>{$reg['ip']}</td>
                <td>
                    <button name='editar' value='{$reg['id']}'>
                        Editar
                    </button>

                    <button name='apagar' value='{$reg['id']}'>
                        apagar
                    </button>
                </td>
			</tr>";
}

echo "</table>";

echo "</form>";




exit();



if( isset($_POST['nome']) ){

	$f = fopen($banco_de_dados, 'a');

	fwrite($f, "{$_POST['nome']};{$_POST['url']};{$_SERVER['REMOTE_ADDR']}\n");

	fclose($f);
}		

echo "<pre>";

if( is_file($banco_de_dados) ){

	$f = fopen($banco_de_dados, 'r');

	while( $linha = fgets($f) ){

		echo $linha;
	}

	fclose($f);

}

echo "</pre>";


?>

</div>
    
</body>
</html>




