<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>


<style>

body{
    background: #eee;
    position: relative;
}

.container{
    width: 50%;
    margin: auto;
}

label{
    margin-bottom: 10px;
    font-weight: 500;
    font-size: 16px;
    color: #662d91;
    font-family: sans-serif;
}

form{
    display: flex;
    flex-direction: column;
    width: 60%;
    margin: 0 auto;
    margin-top: 20%;
}

input{
    background: none;
    padding: 15px;
    border: 2px solid #662d91;
    border-radius: 4px;
    font-size: 16px;
    outline: none;
}

input[type='submit']{
    width: 100px;
    padding: 10px;
    background: #662d91;
    color: #fff;
    font-size: 16px;
    cursor: pointer;
    margin-left: auto;
}

input[type='submit']:hover{
    opacity: 0.8;
}

section{
    /* width: 450px;
    height: 400px;
    top: 15%;
    text-align:center;
    border-radius: 5px;
    box-shadow: 0px 0px 500px 20px rgba(0, 0, 0, 0.2);
    position: fixed; */
    /* background: green; */
    /* color: #fff;
    font-weight: bold;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 22px;
    left: 35%; */
    /* opacity: 0.9; */
}

</style>
<body>


<div class="container">
    <?php


    if( $db = mysqli_connect(	'localhost',
							'root',
							'',
							'aula_php',
							3306) ){
    } else {

        die("Problema ao conectar ao SGDB");
    }

    $email = isset($_POST['nome']) ? $_POST['nome'] : null;		
    $senha = isset($_POST['senha']) ? $_POST['senha'] : null;
    $login = null;

   


        if ( isset($_POST['email']) ) {

            //Faz consulta preparada para evitar SQL injection
            $objConsulta = mysqli_prepare( $db, 'SELECT id, nome, senha FROM tb_usuario WHERE email=?');
        
            mysqli_stmt_bind_param(	$objConsulta, 's', $_POST['email']);

            mysqli_stmt_execute($objConsulta);

            $result = mysqli_stmt_get_result($objConsulta);

            $existe = $result->num_rows;

            

            $usuario = $result->fetch_assoc();


            if( $existe > 0 && $_POST['password'] == $usuario['senha'] ){
                $login = true;
            } else {
                $login = false;
            }

            if($login === true){
                session_start();

                $_SESSION['id_usuario'] = $usuario['id'];

                $_SESSION['nome_usuario'] = $usuario['nome'];

                require('menu.php');

                exit();

                echo "<section>Usuario encontrado! VOCÊ ESTÁ LOGADO<section>";
            } elseif ($login === null) {
                echo "NÃO ENCONTRAMOS O USUÁRIO";
            } else {
                echo "VERIFIQUE SUA SENHA";
            }


        }


        echo "<form method='post'>
        <label>E-mail</label>
        <input type='email' name='email''><br>
        <label>Senha:</label> 
        <input type='password' name='password'><br>
        <input type='submit' value='salvar'>
    </form>";

        
    ?>

</div>
    
</body>
</html>


