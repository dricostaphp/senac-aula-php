<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>

body{
    padding: 0;
    margin: 0;
}

nav{
    width: 100%;
    background: #662d91;
    position: fixed;
    left: 0;
}

ul{
    list-style: none;
    display: flex;
    align-items: center;
}

li{
    margin: 0px 10px;
    color: #FFF;
    font-size: 16px;
    font-family: Arial, sans-serif;
    cursor: pointer;
}

a{
    text-decoration: none;
    color: #fff;
}

li:hover{
    opacity: 0.7;
}

</style>


<body>

<?php 

   if ( session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
   }
   
   if( !isset($_SESSION['id_usuario']) ){
    header('Location: ../login.php');

    exit();
}


    echo "<nav>
           <ul>
            <li><a href='usuario/'>Usuario</a></li>
            <li>Sobre</li>
            <li>Contato</li>
            <li>Sair</li>
           </ul>
         </nav>";

    ?>
    
</body>
</html>